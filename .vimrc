" Vundle #https://github.com/VundleVim/Vundle.vim
set nocompatible              " be iMproved, required, prevents VIM from trying to be compatible with VI
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Keep Plugin commands between vundle#begin/end.

" ****** VUNDLE EXAMPLES ******
"
" The following are examples of different formats supported:

" plugin on GitHub repo
" 	Plugin 'tpope/vim-fugitive'

" plugin from http://vim-scripts.org/vim/scripts.html
" 	Plugin 'L9'

" Git plugin not hosted on GitHub
" 	Plugin 'git://git.wincent.com/command-t.git'

" git repos on your local machine (i.e. when working on your own plugin)
" 	Plugin 'file:///home/gmarik/path/to/plugin'

" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" 	Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" 	Plugin 'ascenator/L9', {'name': 'newL9'}

" PLUGINS GO HERE
"==================================

" Nerdtree #https://github.com/scrooloose/nerdtree
Plugin 'scrooloose/nerdtree'

" Syntastic #https://github.com/scrooloose/syntastic 
Plugin 'scrooloose/syntastic'

" VIM-JavaScript #https://github.com/pangloss/vim-javascript
" not as good as Solarized
" Plugin 'pangloss/vim-javascript'

" Solarized #https://github.com/altercation/vim-colors-solarized
Plugin 'altercation/vim-colors-solarized'

" VIM Airline #https://github.com/bling/vim-airline
Plugin 'vim-airline/vim-airline'

"==================================
" END PLUGINS

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


syntax enable

" ****** PLUGIN OPTIONS ********

" Syntastic options

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0


" Solarized options

set t_Co=16
set background=dark
let g:solarized_contrast="normal"
let g:solarized_termcolors=256

" 'colorscheme solarized' must appear after all other Solarized options
colorscheme solarized

" ****** END PLUGIN OPTIONS ******



" ***** VIM OPTIONS *****


" Set copy/paste limit. 20 = required setting, <1000 = 1000 lines max, s1000 = 1000Kb limit
set viminfo='20,<1000,s1000


" Show line indents via the Indent Guides plugin #https://github.com/nathanaelkane/vim-indent-guides
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=black
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=darkgrey


" Automatically switch off highlighting when in Insert mode
autocmd InsertEnter * :setlocal nohlsearch
autocmd InsertLeave * :setlocal hlsearch


" Show line numbers
set number


" Show cursor as a line
set cursorline


" Set the tab distance to 4 spaces
set tabstop=4
set shiftwidth=4



" ******* KEY MAPPING *******

" Show <leader> key commands http://stackoverflow.com/questions/1764263/what-is-the-leader-in-a-vimrc-file
set showcmd


" Remap the <leader> key to be the spacebar
let mapleader = " "
" Preview any mappings that start with 'space', to prevent delays when typing in Insert mode #https://github.com/junegunn/dotfiles/issues/2
" imap <leader>


" Search highlighting #https://sanctum.geek.nz/arabesque/vim-search-highlighting/
set hlsearch
nnoremap <leader>j :set incsearch!<CR>
nnoremap <leader>h :set hlsearch!<CR>


" Navigating tabs #https://www.quora.com/How-do-I-switch-between-tabs-in-vim
" NOTE: Tabs are different to Buffers; 
" Tabs are individual windows for individual files. Buffers are multiple views of the same file. 
" This is for opening new tabs or switching between tabs
nnoremap <F2> :tabprevious<CR>
nnoremap <F3> :tabnext<CR>
nnoremap <C-t> :tabnew<CR>
inoremap <F2> <Esc>:tabprevious<CR>i
inoremap <F3> <Esc>:tabnext<CR>i
inoremap <C-t> <Esc>:tabnew<CR>
 
" ctrl-I to switch between vertical or horizontal splitted tabs
map <C-I> <C-W><C-W>
 
" vim explorer
map <F4> :Explore<CR>

" ***** END KEY MAPPINGS *****

