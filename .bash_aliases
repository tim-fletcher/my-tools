# Shorthand alias commands

# NOTES
#
# 1. Use "alias a='b'" with no spaces, or it interprets each gap as a new command and will break
# 2. If the Numberpad is not working in VIM, in Putty go Terminal > Features > Disable application keypad mode


# Grab local environment information
#
#	Create the file '.bash_variables' if it doesn't already exist, in the same directory as .bash_aliases.
#   You can also include additional aliases in .bash_local, again create if if you want it.
#	Variables can be accessed using "$variable" - include the double quotation marks.

source ~/.bash_variables
source ~/.bash_local


# Global variables

REPOS=~/repositories/
SCRIPTS=$REPOS/scripts


# Refresh aliases without needing to log out
 
alias sba='source ~/.bash_aliases'
alias sbr='source ~/.bashrc'
alias sc='vim ~/.bash_aliases'
alias vbr='vim ~/.bashrc'
alias vbv='vim ~/.bash_variables'

#alias svc='sudo su - "$group_user"' - requires explicit permission to run as root
alias svc='sudo -u "$group_user" bash'



# --- Navigation ---

alias xx='exit'
alias ..='cd ..'
# use coreutils to provide GNU implementation of commands on OSX - https://superuser.com/questions/109537/how-to-sort-first-directories-then-files-etc-when-using-ls-in-unix#answer-768486
alias aa='gls -alhF --group-directories-first'
alias cc='clear'
alias me='cd;'
alias hh='history'
# Open a folder of whatever directory is open in the shell
alias open='xdg-open .'



# --- Permissions ---

# Examine file and folder permissions using octal numbering (755, 644 etc)

# append the desired file/folder
alias perm='stat -c "%a %n"'


# Reset file and folder permissions to stock standard

# Files
chfile () {
	chmod 644 "$1"
}

# Directories
chdir () {
	chmod 755 "$1"
}



# --- Safeguards ---

alias rm='echo "*** You are about to IRRETRIEVEABLY DESTROY this thing, CHECK IT IS THE DESIRED TARGET BEFORE PROCEEDING!!! *** " && rm -iv'




# --- Git ---

# Variables - note no double-quotes, as these wrap the output in " "

# commitFormat='%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'
commitFormat='%C(yellow)%h%Creset %C(cyan)%ad%Creset %Creset:%d %s - %C(magenta)%an%Creset'

# Date formats: https://www.php.net/manual/en/datetime.formats.date.php
# Time formats: https://www.php.net/manual/en/datetime.formats.time.php

# Use %H:%M:%I for 24 HH:MM:SS
dateFormat='%Y-%m-%d %H:%M'

# Aliases

alias gs='git status'
alias ga='git add'
alias gc='git commit'
# alias gp='git push' // See 'function gp' below
alias gpt='git push --tags'
alias gpu='git pull --ff-only'
alias gft='git fetch --tags'
alias gm='git merge'
alias gma='git merge --abort'
alias gch='git checkout'
alias gl='git log --graph --decorate=full --date=format:"$dateFormat" --pretty=format:"$commitFormat" --abbrev-commit'
#alias gl='git log --graph --decorate=full --abbrev-commit --oneline --pretty=format:"%h: %<(10)%an - %d %s - %ad"'
alias gd='git diff'
alias glu='git log @{u}..'
alias gt='git tag'


# Selectively push to Github mirrors for various projects

gp () {
    if [[ "$PWD" == "/Users/tim/repositories/parks_australia/parksaustralia-cms" || "$PWD" == "/Users/tim/Localdev/parksaustralia-cms" ]]; then
        echo -e 'Pushing to Pantheon and GitHub mirror\n'
        git push && git push --tags; git push github
    else
        git push
    fi
}

# gb: show all local branches, which tracks which, and the commit at the HEAD of each
alias gb='git branch -vv'

#gba: same as 'gb', but including remotes
alias gbr='git branch -vva'

# gba: show the details of branch, including the author and date last modified
alias gba='git for-each-ref --format='"'"'%(committerdate)%09%(authorname)%09%(refname)'"'"' | sort -k5n -k2M -k3n -k4n | grep remotes | awk -F "\t" '"'"'{ printf "%-32s %-27s %s\n", $1, $2, $3 }'"'"' '

# gpab: Automatically create matching local branches for all remotes, and set up tracking
alias gpab='git branch -r | grep -v '"'"'\->'"'"' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done | git fetch --all | git pull --all'

# gr: show key details of the repository, including local and remote branches and which branches are being tracked
alias gr='git remote show "$remote_username"'

# grh: discard changes to a file/folder in the working tree, and reset it to it's state in the branch HEAD
alias grh='git reset HEAD'

# gu: undo your last commit, but keep the changes in the working tree
alias gu='git reset --soft HEAD~1'

# glr: show the log history of the remote tracking branch of your current branch - so normally 'origin/master' if running this from the local 'master' branch
alias glr='git fetch --all | git log @{u} --graph --decorate=full --date=format:"$dateFormat" --pretty=format:"$commitFormat" --abbrev-commit'

# gsf: follow with desired commit hash
alias gsf='git diff-tree --no-commit-id --name-only -r'

# glf: follow with desired filename
alias glf='git log --follow --oneline --decorate'

# gsc: show commits present on nominated branch, or all commits present on nominated branch but not on another branch
# e.g. '$ git cherry -v master mybranch' shows all commits on mybranch but not master, while '$git cherry -v mybranch' will show all commits in mybranch and master
alias gsc='git cherry -v'

# gcp: cherry-pick commits to add ot the HEAD of the current branch
alias gcp='git cherry-pick'

# grp: Reset all git files to have Read permissions for all users
alias grp='chmod -R go+r .git/'

# grsp: Logically the same as $ git config --add core.sharedRepository 0664,  Controls default permissions for the repository
alias grsp='git config --add core.sharedRepository all'

# grub: Remove all local tracking branches for which there is no remote set
alias grub='git fetch -p && for branch in `git branch -vv | grep ": gone]" | gawk "{print $1}"`; do git branch -D $branch; done'

# Pull all remote branches with '--ff-only', and show errors for those that can't be fast-forwarded
# Relies on having a script saved from  https://stackoverflow.com/questions/4577874/git-automatically-fast-forward-all-tracking-branches-on-pull#answer-24451300
alias guab='~/pull-all-branches-ff-only.sh'

# Bulk-delete removed items, similar to 'git add *', but for 'rm'
alias grma='git ls-files --deleted -z | xargs -0 git rm'



# --- Docker ---

alias dc='docker-compose'
alias dcccs='docker-compose exec -T test drush cc css-js'
alias dccca='docker-compose exec -T test drush cc all'



# --- Sass ---

# scf: automatically fix indenting of a SASS or CSS file to use indents
alias scf='sass-convert --indent t'

# scc: convert a CSS file to SASS, and use indenting to format it
alias scc='sass-convert -F css -T scss --indent t'



# --- Drush ---

alias dccs='drush cc css-js'
alias dcca='drush cc all'
alias sdcca='sudo drush cc all'

# dmv: list all enabled non-core Drupal modules
alias dmv='drush pm-list --type=module --no-core --status=enabled'
# dusers: list all Drupal accounts and details
alias dusers='drush uinf $(drush sqlq "SELECT GROUP_CONCAT(uid) FROM users")'
# *** DEV SITES ONLY *** grant the administrator ALL permissions for ALL modules
alias dgod='drush eval "user_role_grant_permissions(user_role_load_by_name('administrator')->rid, array_keys(user_permission_get_modules()));"'



# ------ Composer ------

alias composer='php -d memory_limit=-1 /usr/local/bin/composer'


# --- Xidel ---

alias xidel='xidel\ 2'



# --- System ---

# Get operating system details
alias os='lsb_release -a'

# Server usage
alias meminfo='free -m -l -t'

# Get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

# Get top process eating cpu
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

# Get sizes of all directories under the current location and suppress 'permission denied' errors
alias sizes='du -sh * netcdf 2>&-'

# Get server cpu info
alias cpuinfo='lscpu'

# Get GPU ram on desktop/laptop
alias gpumeminfo='grep -i --color memory /var/log/Xorg.0.log'

# Get disk usage
alias diskuse='df -h'


# --- Network ---

# Show my public IP address
alias myip='curl ifconfig.me'

# Show internal network information, including internal my IP address
alias mynet='ifconfig -a'


# --- Search ---

# Search - for function syntax, see http://stackoverflow.com/questions/6212219/passing-parameters-to-a-bash-function

	# Run using 'ff "*.ext" "contents" '
	# $1 is the file name and extension
	# $2 is the text contents within the file

ff () {
	find . -name "$1" | xargs grep -liEs "$2"
}


# Targeting - like searching, but for getting all instances of a string, including file and line number. Based on http://stackoverflow.com/questions/16956810/finding-all-files-containing-a-text-string-on-linux?rq=1

	# Run using 'target "string"'
	# $1 is the string you wish to locate
	# $2 is an expression for files you wish to exclude from results, e.g. '*.css'
	# Best run from the target directory, to save juice

target () {
	grep -rn . --exclude="$2" -e "$1"
}



# --- BackupsS ---

# Backups directories using Tar and gzip 

	#.Prefix with 'sudo -u "$username"' to run with sudo priviledges
	# $1 is the target directory you want to compress
	# $2 is the archive name with no extensions e.g. 'myarchive-YYMMDD' - which turns into 'myarchive-20XX-XX-XX.tar.gz'

backup () {
	tar -zcf "$2".tar.gz "$1"
	mv "$2".tar.gz "$backup_location"
}

# Restoring gz backups made with the command above - retaining original permissions of the archive contents

	# Must be run from within the <backup_location>
	# Prefix with 'sudo -u "$username"' to run with sudo priviledges
	# $1 is the full archive name e.g. 'myfolder.tar.gz'
	# $2 is the absolute path to where you want it placed e.g. /home/user/_backups
	# NOTE: when restoring a site, you will need to run '$ drush cc all' for it to pick up the files	

restore () {
	tar --no-overwrite-dir -zxf "$1" -C "$2"
}



# --- Databases ---

# View all databases in a list

showdb () {
	mysql -h"$database_ip" -u"$database_user" -p"$database_password" -e "show databases;"
}

# View the size of all databases (may take a while to run)

dbsize () {
	mysql -h"$database_ip" -u"$database_user" -p"$database_password" -e "SELECT table_schema 'Data Base Name', sum( data_length + index_length ) / 1024 / 1024 'Data Base Size in MB', sum( data_free )/ 1024 / 1024 'Free Space in MB'		FROM information_schema.TABLES GROUP BY table_schema ;"
}

# View all tables within a nominated database

		# $1 is the database name

showtables () {
	mysql -h"$database_ip" -u"$database_user" -p"$database_password" -e "use \"$1\"; show tables;"
}


# Export databases
# This exports the database, creates a gZipped TAR version, then deletes the exported SQL copy to save space.

        # $1 is the site short name
        # $2 is the backup name

exportdb () {
	mysqldump -h"$database_ip" -u"$database_user" -p"$database_password" "$1" > "$backup_location"/"$2".sql
	tar -zcvf "$backup_location"/"$2".sql.tar.gz "$backup_location"/"$2".sql
	rm "$backup_location"/"$2".sql
}


# Import database
# This imports a given SQL file into an SQL database

        # $1 is the file you wish to import, with no extensions
        # $2 is the name of the database into which you are importing $1

importdb () {
	mysql -h"$database_ip" -u"$database_user" -p"$database_password" -e "$2" < "$1".sql
}


