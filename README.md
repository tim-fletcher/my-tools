# Limux Server Tools (LST)

**For scripts, check out Tim's [Github and gists](https://github.com/T-Fletcher?tab=repositories)**

All files within should reside in the user's root directory (normally `/User/username` on Mac/Linux, or `/home/username` on Windows)

The `.gitignore` file explicitly ignores everything except the files in this repo, meaning you can 
extract the repo files into any directory you want (so long as it's not already a git repo)

This collection of files presets a bunch of stuff to make Linux and VIM easier to use, including:

1. `.bash_aliases`        - shortcuts and functions to speed up navigation and system management
2. `.bash_variables.sh`   - stores variables unique to your server environment, which are then used by .bash_aliases
3. `.bashrc`              - presets the shell behaviour with colours and lots of handy stuff - try running '$ history'
4. `.gitconfig`           - predefines the Git user and git log colours
5. `.vim/`                - contains the Solarized colour scheme for VIM, which is great on the eyes
6. `.vimrc`               - presets VIM with some standard features, such as line numbers, colours and more

To use all this, open `.gitconfig` and update the name and email to your own! 
